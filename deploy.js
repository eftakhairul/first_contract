//Rinkeby https://rinkeby.infura.io/vE1vwic8B0VMsXAjxAsP
// https://rinkeby.etherscan.io/address/0x8b1c9aece34f53234ae948c92c1eb716be20d561

const HDWalletProvider        = require('truffle-hdwallet-provider');
const Web3                    = require('web3');
const {interface, bytecode}   = require('./compile');


// interfecr to real rinkeby network
const provider                = new HDWalletProvider(
  'just devote goat fancy finger kid capital antenna never tell feed siren',
  'https://rinkeby.infura.io/vE1vwic8B0VMsXAjxAsP'
);
const web3                    = new Web3(provider);

// deploy method to real network
const deployContract = async () => {
  const accounts = await web3.eth.getAccounts();
  console.log('Attempting to deploy from the account', accounts[0]);
  const result = await new web3.eth.Contract(JSON.parse(interface))
                                   .deploy({ data: bytecode, arguments: ['Hello there for rinkeby'] })
                                   .send({ from: accounts[0], gas: '1000000' });
  console.log('Contract deployed to ', result.options.address);                                
};

deployContract();
