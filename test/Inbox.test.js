const assert    = require('assert');
const ganache   = require('ganache-cli');
const Web3      = require('web3');
const provider  = ganache.provider();
const web3      = new Web3(provider);

const {interface, bytecode} = require('../compile');

let accounts;
let inboxContract;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  //deploy smart contact with initial message
  inboxContract = await new web3.eth.Contract(JSON.parse(interface))
                                    .deploy({ data: bytecode, arguments: ['hellow world smart contract'] })  // if you don't have message, you can skip arguments
                                    .send({ from: accounts[0], gas: '1000000' });

  inboxContract.setProvider(provider);     
});

describe('Inbox', () => {
  it('deploy a contract', () => {
    assert.ok(inboxContract.options.address);   
  });

  it('has a default message', async () => {
    const message = await inboxContract.methods.message().call();
    assert.equal(message, 'hellow world smart contract');
  });

  it('can change the default message', async () => {
    const tx = await inboxContract.methods.setMessage('hey hello world').send({ from: accounts[0] });  //to send money need to "value: web3.utils.toWei('0.01', 'ether')"
    assert.ok(tx);  
     
    const message = await inboxContract.methods.message().call();  // you can pass "from: accounts[0]"" like above 
    assert.equal(message, 'hey hello world');
  });

  //example od something went wrong
  xit('error handling', async () => {
    try {
      await lottery.methods.enter().send({
        from: accounts[0], 
        value: 0
      });

      assert(false);
    } catch(err) {
      assert(err);
    }
  });

  //example od something went wrong
  xit('error handling for pickwinner', async () => {
    try {
      await lottery.methods.pickWinner().send({
        from: accounts[1]        
      });
      assert(false);
    } catch(err) {
      assert(err);
    }
  });


  //example od something went wrong
  xit('full life circle', async () => {
    await lottery.methods.enter().send({
      from: accounts[0], 
      value: 10000000000,
    });
  });
});